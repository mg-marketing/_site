trimString: function(string) {
    return string.replace(/^\s+|\s+$/g, '');
  },

  parse: {
    date: function(string) {
      let parsed = string.match(/^(\d{4,})-(\d\d)-(\d\d)$/);
      if (!parsed)
        return null;
      let [_, year, month, day] = parsed.map(x => parseInt(x, 10));
      let date = new Date(year, month - 1, day);
      if (date.getFullYear() !== year || date.getMonth() + 1 !== month || date.getDate() !== day)
        return null;
      return date;
    },
    string: function(string) {
      return string;
    },
    integer: function(string) {
      if (isNaN(string))
        return null;
      return parseInt(string, 10);
    },
    number: function(string) {
      if (isNaN(string))
        throw null;
      return parseFloat(string);
    },
    'boolean': function _boolean(string) {
      return !(/^\s*false\s*$/i.test(string));
    },
    object: function(string) {
      return Utils.deserializeValue(string);
    }
  }
