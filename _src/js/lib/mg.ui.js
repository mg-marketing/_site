
// ------ UI ------
// interactive scripts that modify front-end properties

$(function(){

  $('.scrollto').click(function(event) {
      event.preventDefault();
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
          || location.hostname == this.hostname) {

          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          var target_offset = $('header').height() - 4;

             if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top - target_offset
              }, 1000);
              return false;
          }
      }
  });


  $('.scroll-top').click(function(event) {
      event.preventDefault();
      $("html, body").animate({
            scrollTop: 0
      }, "fast");
  });


  $('.popup-video').click(function(event){
      event.preventDefault();
      var product = $(this).attr('data-product');
      loadVimeo(product);
  });

});


function loadVideo(video,host) {

    var v = video;
    var p = provider;
    var src;

    switch (p) {
        case 'yt': src = 'https://www.youtube.com/embed/' + v + '?rel=0&showinfo=0&autoplay=0';
        case 'v': src = 'player.vimeo.com/video/' + v + '?autoplay=1&title=0&byline=0&portrait=0';
    }

    $("body").append('<div class="modal fade bs-example-modal-lg video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-video"><div class="modal-dialog modal-lg" role="document"> <div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">&#xE5CD;</i></button></div><div class="modal-body"><div class="embed-responsive embed-responsive-16by9 mb-2"><iframe class="embed-responsive-item" src="' + src + '" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe></div></div></div></div></div>'), $("#modal-video").modal("show"), $("#modal-video").on("hidden.bs.modal", function() {
        $("#modal-video").remove();
    });
}

function loadVimeo(product) {

    var video;

    if ( product == 'mini' ) {
      video = 442100882;
    } else if ( product == 'classic' ) {
      video = 441906506;
    } else if ( product == 'home' ) {
      video = 441906878;
    } else if ( product == 'active' ) {
      video = 441906216;
    } else if ( product == 'mobile' ) {
      video = 441906943;
    } else if ( product == 'freedom' ) {
      video = 279878145;
    }


    $("body").append('<div class="modal fade bs-example-modal-lg video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-video"><div class="modal-dialog modal-lg p-0" role="document"> <div class="modal-content p-0"><div class="modal-header p-0"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">&#xE5CD;</i></button></div><div class="modal-body p-0"><div class="embed-responsive embed-responsive-16by9 mb-2"><iframe src="https://player.vimeo.com/video/'+ video +'?autoplay=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe><script src="https://player.vimeo.com/api/player.js"></script></div></div></div></div></div>'), $("#modal-video").modal("show"), $("#modal-video").on("hidden.bs.modal", function() {
        $("#modal-video").remove();
    });

}
