module.exports = function(grunt) {

  grunt.initConfig({
    concat: {
      options: {
        separator: ';'
      },
      bootstrap: {
        src: ['_src/js/vendor/lib/bootstrap/src/**/*.js'],
        dest: '_dist/js/bootstrap.js'
      },
      plugins: {
        src: ['_src/js/vendor/plugins/slick/slick.min.js','_src/js/vendor/plugins/sticky-sidebar.min.js','_src/js/vendor/plugins/smooth-scroll/smooth-scroll.polyfills.min.js'],
        dest: '_dist/js/plugins.js'
      },
      app: {
        src: ['_src/js/lib/mg.ui.js'],
        dest: '_dist/js/app.js'
      }
    },
    uglify: {
      dist: {
        bootstrap: {
          '_dist/js/bootstrap.min.js' : ['_dist/js/bootstrap.js']
        },
        app: {
          '_dist/js/app.min.js' : ['_dist/js/app.js']
        }
      }
    },
    sass: {
        core: {
            files: {
                '_dist/css/styles.css' : '_src/scss/styles.scss'
            }
        },
        plugins: {
            files: {
                '_dist/css/plugins.css' : ['_src/scss/vendors/slick/slick.scss','_src/scss/vendors/slick/slick-theme.scss']
            }
        },
        pages: {
            files: {
                '_dist/css/buying_advice.css' : '_src/scss/pages/buying_advice.scss'
            }
        },
        products: {
            files: {
                '_dist/css/products.css' : '_src/scss/pages/products.scss'
            }
        },
        checkout: {
            files: {
                '_dist/css/checkout.css' : '_src/scss/pages/checkout.scss'
            }
        }
    },
    watch: {
        css: {
            files: '_src/scss/**/*.scss',
            tasks: ['sass']
        },
        app: {
            files: '_src/js/lib/**/*.js',
            tasks: ['concat:app']
        },
        plugins: {
            files: '_src/js/vendor/plugins/**/*.js',
            tasks: ['concat:plugins']
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-critical');

  grunt.registerTask('default', ['concat','sass','uglify','watch']);
  grunt.registerTask('checkout', ['concat','sass','uglify','watch']);
  grunt.registerTask('scripts', ['concat','watch:app','watch:plugins']);
};
